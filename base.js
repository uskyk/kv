function handleTextChange(event) {
	const target = event.target;
	const value = target.value;
	if (value.length == 0) {
		target.value = "-";
	} else {
		target.value = value.substr(value.length - 1);
	}
}
function handleTextScroll(event) {
	const target = event.target;
	const value = target.value;
	if (event.deltaY < 0) {
		target.value = "1"; // scroll up
	} else {
		target.value = "0"; // scroll down
	}
	event.preventDefault();
}
export function attachTypeListener(element) {
	element.addEventListener("input", handleTextChange);
	element.addEventListener("wheel", handleTextScroll);
}

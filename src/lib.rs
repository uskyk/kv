use std::{fmt, str::FromStr};

use itertools::Itertools;

mod ui;
use log::{debug, trace, warn};
pub use ui::*;

pub type Variable = usize;
pub type Block = Vec<(Variable, Output)>;
pub type BlockRef<'a> = &'a [(Variable, Output)];

pub const A: Variable = 1 << 0;
pub const B: Variable = 1 << 1;
pub const C: Variable = 1 << 2;
pub const D: Variable = 1 << 3;
pub const E: Variable = 1 << 4;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Output {
	Zero,
	One,
	Any
}

pub use Output::*;

impl Output {
	fn to_num(self) -> usize {
		match self {
			Zero => 0,
			One => 1,
			Any => todo!()
		}
	}

	fn invert(self) -> Self {
		match self {
			Zero => One,
			One => Zero,
			Any => Any
		}
	}
}

impl From<usize> for Output {
	fn from(x: usize) -> Self {
		match x {
			0 => Zero,
			1 => One,
			_ => Any
		}
	}
}

impl From<char> for Output {
	fn from(x: char) -> Self {
		match x {
			'0' => Zero,
			'1' => One,
			_ => Any
		}
	}
}

impl FromStr for Output {
	type Err = ();

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Ok(match s {
			"0" => Zero,
			"1" => One,
			_ => Any
		})
	}
}

impl fmt::Display for Output {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Zero => write!(f, "0"),
			One => write!(f, "1"),
			Any => write!(f, "-")
		}
	}
}

impl Into<char> for Output {
	fn into(self) -> char {
		match self {
			Zero => '0',
			One => '1',
			Any => '-'
		}
	}
}

pub fn parse_function(func: &str) -> FunctionSpec {
	func.chars().map(|x| x.into()).collect()
}

pub type FunctionSpec = Vec<Output>;

pub fn find_groups(func: &FunctionSpec, vars: &[Variable], typ: Output) -> Vec<Vec<(Variable, Output)>> {
	let mut groups: Vec<Vec<(Variable, Output)>> = Vec::new();
	// k = 0
	if func.iter().all(|&x| x != typ.invert()) {
		groups.push(vec![]);
		return groups;
	}
	for k in 1..vars.len()+1 {
		for vars in vars.iter().combinations(k) {
			for var_bits in 0..(1 << k) {
				let var_setting: Vec<_> = vars
						.iter()
						.enumerate()
						.map(|(i, &&var)| (var, ((var_bits >> i) & 1).into()))
						.collect();
				if groups.iter().any(|x| subset_of(x, &var_setting)) {
					continue;
				}
				let mut mask = 0;
				let mut inv_mask = !0;
				for (i, &&var) in vars.iter().enumerate() {
					mask |= if ((var_bits >> i) & 1) == 1 { var } else { 0 };
					inv_mask ^= if ((var_bits >> i) & 1) == 1 { 0 } else { var };
				}
				let mut fits = true;
				let mut found_typ = false;
				for input in 0..func.len() {
					if check_mask(input, mask, inv_mask) {
						if func[input] == typ.invert() {
							fits = false;
							break;
						} else if func[input] == typ {
							found_typ = true;
						}
					}
				}
				if fits && found_typ {
					groups.push(var_setting);
				}
			}
		}
	}
	groups
}

pub fn find_core<'a>(func: &FunctionSpec, vars: &[Variable], typ: Output, blocks: &'a [Block]) -> (Vec<&'a [(Variable, Output)]>, Vec<&'a [(Variable, Output)]>) {
	assert_eq!(func.len(), 2usize.pow(vars.len() as u32));

	let block_masks = blocks.iter()
		.map(block_to_mask)
		.collect::<Vec<_>>();

	let mut prime = vec![false; blocks.len()];
	'input: for input in 0..func.len() {
		if func[input] != typ {
			continue;
		}
		let mut matched = None;
		for (i, &(mask, inv_mask)) in block_masks.iter().enumerate() {
			if check_mask(input, mask, inv_mask) {
				if matched.is_none() {
					matched = Some(i);
				} else {
					continue 'input;
				}
			}
		}
		if let Some(i) = matched {
			prime[i] = true;
		}
	}
	let a = prime.iter().enumerate().filter(|&(_, &prime)| prime).map(|(i, _)| &*blocks[i]).collect();
	let b = prime.iter().enumerate().filter(|&(_, &prime)| !prime).map(|(i, _)| &*blocks[i]).collect();
	(a, b)
}

/// May return some incomplete solutions
pub fn all_solutions(func: FunctionSpec, vars: &[Variable], typ: Output, core: &[BlockRef], other: &[BlockRef]) -> Vec<Vec<Block>> {
	all_solutions_idxed(func, vars, typ, core, other)
		.into_iter()
		.map(|sol| sol.into_iter().map(|i| other[i].to_vec()).collect())
		.collect()
}

pub fn real_solutions_idxed(func: FunctionSpec, vars: &[Variable], typ: Output, core: &[BlockRef], other: &[BlockRef]) -> Vec<Vec<usize>> {
	let core_masks = core.iter()
		.map(block_to_mask)
		.collect::<Vec<_>>();
	let other_masks = other.iter()
		.map(block_to_mask)
		.collect::<Vec<_>>();
	let mut solutions = all_solutions_idxed(func.clone(), vars, typ, core, other);
	solutions.sort_unstable_by_key(|x| x.len());
	solutions.into_iter()
		.filter(|sol|
			(0..func.len())
				.filter(|&i| func[i] == typ)
				.all(|i|
					core_masks.iter()
						.chain(sol.iter().map(|&x| &other_masks[x]))
						.any(|&(mask, inv_mask)| check_mask(i, mask, inv_mask)))
		).collect()
}

pub fn all_solutions_idxed(mut func: FunctionSpec, vars: &[Variable], typ: Output, core: &[BlockRef], other: &[BlockRef]) -> Vec<Vec<usize>> {
	assert_eq!(func.len(), 2usize.pow(vars.len() as u32));

	// first mark all inputs covered by core blocks as Any
	let prime_masks = core.iter()
		.map(block_to_mask)
		.collect::<Vec<_>>();
	'input: for input in 0..func.len() {
		if func[input] != typ {
			continue;
		}
		for &(mask, inv_mask) in prime_masks.iter() {
			if check_mask(input, mask, inv_mask) {
				func[input] = Any;
				continue 'input;
			}
		}
	}
	// then start recursive search
	let other_masks = other.iter()
		.map(block_to_mask)
		.collect::<Vec<_>>();
	all_recursive(func, vars, typ, other, &other_masks, 0)
}

fn all_recursive(func: FunctionSpec, vars: &[Variable], typ: Output, other: &[BlockRef], other_masks: &[(Variable, Variable)], start: usize) -> Vec<Vec<usize>> {
	let mut all = Vec::new();
	'block: for i in start..other.len() {
		trace!("trying {}", i);
		let (mask, inv_mask) = other_masks[i];
		for input in 0..func.len() {
			if func[input] != typ {
				continue;
			}
			if check_mask(input, mask, inv_mask) {
				trace!("success by idx {}", input);
				// this block is useful
				// mark inputs as done
				let mut func = func.clone();
				for input in 0..func.len() {
					if check_mask(input, mask, inv_mask) {
						trace!("set {} to any", input);
						func[input] = Any;
					}
				}
				let extensions = all_recursive(func, vars, typ, other, other_masks, i+1);
				if extensions.is_empty() {
					all.push(vec![i]);
				}
				for mut extension in extensions {
					if !extension.is_empty() {
						extension.push(i);
						all.push(extension);
					} else {
						warn!("empty extension?");
					}
				}
				continue 'block;
			}
		}
	}
	trace!("returning");
	all
}

/// a ⊆ b
fn subset_of<T: Eq>(a: &[T], b: &[T]) -> bool {
	a.iter().all(|x| b.contains(x))
}

pub fn block_to_mask<T: AsRef<[(Variable, Output)]>>(block: T) -> (Variable, Variable) {
	let mut mask = 0;
	let mut inv_mask = !0;
	for &(var, out) in block.as_ref() {
		mask |= if out == One { var } else { 0 };
		inv_mask ^= if out == Zero { var } else { 0 };
	}
	(mask, inv_mask)
}

#[inline(always)]
pub fn check_mask(input: usize, mask: usize, inv_mask: usize) -> bool {
	input | mask == input && input & inv_mask == input
}
pub fn print_mf<'a, I>(x: I) -> String
where I: IntoIterator<Item = &'a[(Variable, Output)]>
{
	let mut text = String::new();
	for x in x {
		if !text.is_empty() {
			text += ") ∧ (";
		} else {
			text.push('(');
		}
		text += &print_implicate(x);
	}
	if !text.is_empty() {
		text.push(')');
	}
	text
}

pub fn print_implicate(vars: &[(Variable, Output)]) -> String {
	let mut s = String::new();
	for i in 0..vars.len() {
		let (var, out) = vars[i];
		s.push(print_var(var));
		if out == One {
			s += "̅";
		}
		if i != vars.len() - 1 {
			s += " ∨ ";
		}
	}
	s
}

pub fn print_df<'a, I>(x: I) -> String
where I: IntoIterator<Item = &'a[(Variable, Output)]>
{
	let mut text = String::new();
	for x in x {
		if !text.is_empty() {
			text += " ∨ ";
		}
		text += &print_implicant(x);
	}
	text
}

pub fn print_implicant(vars: &[(Variable, Output)]) -> String {
	let mut s = String::new();
	for i in 0..vars.len() {
		let (var, out) = vars[i];
		s.push(print_var(var));
		if out == Zero {
			s += "̅";
		}
		if i != vars.len() - 1 {
			s += " ";
		}
	}
	s
}

fn print_var(var: Variable) -> char {
	('a' as u8 + (0usize.leading_zeros() - var.leading_zeros() - 1) as u8) as char
}

#[test]
fn test_3var() {
	let fun: FunctionSpec = vec![0, 0, 1, 0, 0, 1, 1, 1].into_iter().map(Into::into).collect();
	let groups = find_groups(&fun, &mut [A, B, C], Zero);
	assert_eq!(groups, vec![
		vec![(A, Zero), (B, Zero)],
		vec![(A, One), (C, Zero)],
		vec![(B, Zero), (C, Zero)],
	]);
	let prime = find_core(&fun, &mut [A, B, C], Zero, &groups).0;
	assert_eq!(prime, vec![
		&[(A, Zero), (B, Zero)],
		&[(A, One), (C, Zero)],
	]);
}

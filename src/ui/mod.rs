pub mod svg;

/// Returns grid coordinates, width, height
pub fn grid(var_count: usize) -> (Vec<(usize, usize)>, usize, usize) {
	let mut grid = vec![(0, 0), (1, 0)];
	let mut h = 1;
	let mut w = 2;

	for _ in 1..var_count {
		if w > h {
			h *= 2;
			let prev_grid_size = grid.len();
			for i in 0..prev_grid_size {
				let mut cell = grid[i];
				cell.1 = h - 1 - cell.1;
				grid.push(cell);
			}
		} else {
			w *= 2;
			let prev_grid_size = grid.len();
			for i in 0..prev_grid_size {
				let mut cell = grid[i];
				cell.0 = w - 1 - cell.0;
				grid.push(cell);
			}
		}
	}

	(grid, w, h)
}

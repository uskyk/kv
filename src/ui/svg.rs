use svg_fmt::*;

use crate::*;

const SIZE_FONT: f32 = 20.0;

const COLORS: &'static [Color] = &[
	rgb(0, 170, 255),
	rgb(255, 0, 0),
	rgb(0, 255, 0),
	rgb(255, 170, 0),
	rgb(255, 0, 255), // pink
	rgb(0, 128, 0), // dark green
	rgb(0, 0, 128), // dark blue
	rgb(128, 0, 128), // dark red
];

pub fn get_svg(size_factor: usize, print_labels: bool,
	function: &FunctionSpec, vars: &[Variable], block_masks: &[(usize, usize)]) -> String {
	let size2 = (size_factor / 2) as f32;
	let size4 = size_factor / 4;
	let size8 = size_factor / 8;
	let size16 = (size_factor / 16) as f32;
	let delta = size_factor as f32 / 32.0;
	let mut svg = String::new();

	let (grid, w, h) = grid(vars.len());

	let mut blocks = vec![vec![vec![]; w]; h];
	for (i, &(x, y)) in grid.iter().enumerate() {
		for (idx, &(mask, inv_mask)) in block_masks.iter().enumerate() {
			if check_mask(i, mask, inv_mask) {
				blocks[y][x].push(idx);
			}
		}
	}

	let (start_x, start_y, w_add, h_add) = if print_labels {
		let mut a = 0.0;
		let mut b = 0.0;
		let mut c = 0.0;
		let mut d = 0.0;
		for i in 0..vars.len() {
			match i % 4 {
				0 => {
					b -= size2;
					d += size2;
				},
				1 => {
					a -= size2;
					c += size2;
				},
				2 => {
					d += size2;
				},
				3 => {
					c += size2;
				},
				_ => unreachable!()
			}
		}
		(a, b, c, d)
	} else {
		(0.0, 0.0, 0.0, 0.0)
	};

	let w_base = (w * size_factor + 3) as f32;
	let h_base = (h * size_factor + 3) as f32;
	svg += &BeginSvg {
		w: w_base + w_add, h: h_base + h_add,
		x: start_x, y: start_y
	}.to_string();

	if print_labels {
		for i in 0..vars.len() {
			let offset = (i / 4) as f32 * size2 + size16;
			let initial_window = 2usize.pow((i / 2) as u32);
			//eprintln!("i = {i}, offset = {offset}, initial_window = {initial_window}");
			let mut start = vec![false; initial_window];
			start.extend(vec![true; initial_window]);
			let desired_len = if i % 2 == 0 { w } else { h };
			while start.len() < desired_len {
				let j = start.len();
				for k in 0..j {
					start.push(start[j-k-1]);
				}
			}
			let mut segments = vec![];
			let mut j = 0;
			while j < desired_len {
				if let Some((idx, _)) = start[j..].iter().find_position(|x| **x) {
					j += idx;
					let j2 = start[j..].iter().find_position(|x| !**x).map(|x| x.0).unwrap_or(desired_len-j);
					segments.push((j, j+j2-1));
					j += j2;
				} else {
					break;
				}
			}
			let var_name = print_var(vars[i]);
			match i % 4 {
				0 => {
					for (x1, x2) in segments {
						svg += &line_segment(x1 * size_factor, -offset, (x2 + 1) * size_factor, -offset)
							.width(2.0)
							.to_string();
						svg += &text((x1 + x2 + 1) * size_factor / 2, -offset - size16, var_name)
							.align(Align::Center)
							.size(SIZE_FONT)
							.to_string();
					}
				},
				1 => {
					for (y1, y2) in segments {
						svg += &line_segment(-offset, y1 * size_factor, -offset, (y2 + 1) * size_factor)
							.width(2.0)
							.to_string();
						svg += &text(-offset - size8 as f32, (y1 + y2 + 1) * size_factor / 2 + SIZE_FONT as usize / 2, var_name)
							.align(Align::Center)
							.size(SIZE_FONT)
							.to_string();
					}
				},
				2 => {
					for (x1, x2) in segments {
						svg += &line_segment(x1 * size_factor, h_base + offset, (x2 + 1) * size_factor, h_base + offset)
							.width(2.0)
							.to_string();
						svg += &text((x1 + x2 + 1) * size_factor / 2, h_base + offset + size4 as f32, var_name)
							.align(Align::Center)
							.size(SIZE_FONT)
							.to_string();
					}
				},
				3 => {
					for (y1, y2) in segments {
						svg += &line_segment(w_base + offset, y1 * size_factor, w_base + offset, (y2 + 1) * size_factor)
							.width(2.0)
							.to_string();
						svg += &text(w_base + offset + size8 as f32, (y1 + y2 + 1) * size_factor / 2 + SIZE_FONT as usize / 2, var_name)
							.align(Align::Center)
							.size(SIZE_FONT)
							.to_string();
					}
				},
				_ => unreachable!()
			}
		}
	}

	for (i, &(x, y)) in grid.iter().enumerate() {
		svg += &rectangle(1 + x * size_factor, 1 + y * size_factor, size_factor, size_factor)
				.fill(white())
				.stroke(Stroke::Color(black(), 2.0)).to_string();
		let x_start = 1 + x * size_factor;
		let x_end = 1 + (x+1) * size_factor;
		let y_start = 1 + y * size_factor;
		let y_end = 1 + (y+1) * size_factor;
		let mut rect = rectangle(1 + x * size_factor, 1 + y * size_factor, size_factor, size_factor)
			.fill(white())
			.stroke_opacity(0.7)
			.inflate(-1, -1);
		for (idx, &(mask, inv_mask)) in block_masks.iter().enumerate() {
			rect = rect.inflate(-delta, -delta);
			if check_mask(i, mask, inv_mask) {
				// check cells around this one
				let mut up = false;
				let mut down = false;
				let mut left = false;
				let mut right = false;
				for &var in vars {
					let j = i ^ var;
					if check_mask(j, mask, inv_mask) {
						let (x2, y2) = grid[j];
						if (x + 1) % w == x2 && y == y2 {
							right = true;
						}
						if x == x2 && (y + 1) % h == y2 {
							down = true;
						}
						if x == x2 && y == (y2 + 1) % h {
							up = true;
						}
						if x == (x2 + 1) % w && y == y2 {
							left = true;
						}
						if !up && !down && !left && !right {
							// happens when vars >= 5 (obviously)
						}
					}
				}
				let mut sides = rect.sides();
				for s in &mut sides {
					*s = s.opacity(0.9);
				}
				let up = up && !(y == 0 && (0..h).all(|y| blocks[y][x].contains(&idx)));
				let down = down && !(y == h-1 && (0..h).all(|y| blocks[y][x].contains(&idx)));
				let left = left && !(x == 0 && (0..w).all(|x| blocks[y][x].contains(&idx)));
				let right = right && !(x == w-1 && (0..w).all(|x| blocks[y][x].contains(&idx)));
				if up {
					svg += &sides[0].width(2.0).color(white()).to_string();
					if !left {
						svg += &sides[2].width(2.0).color(COLORS[idx % COLORS.len()]).flip_to(0.0, y_start).to_string();
					}
					if !right {
						svg += &sides[3].width(2.0).color(COLORS[idx % COLORS.len()]).flip_to(0.0, y_start).to_string();
					}
				} else {
					svg += &sides[0].width(2.0).color(COLORS[idx % COLORS.len()]).to_string();
				}
				if down {
					svg += &sides[1].width(2.0).color(white()).to_string();
					if !left {
						svg += &sides[2].width(2.0).color(COLORS[idx % COLORS.len()]).flip_to(0, y_end).to_string();
					}
					if !right {
						svg += &sides[3].width(2.0).color(COLORS[idx % COLORS.len()]).flip_to(0, y_end).to_string();
					}
				} else {
					svg += &sides[1].width(2.0).color(COLORS[idx % COLORS.len()]).to_string();
				}
				if left {
					svg += &sides[2].width(2.0).color(white()).to_string();
					if !up {
						svg += &sides[0].width(2.0).color(COLORS[idx % COLORS.len()]).flip_to(x_start, 0).to_string();
					}
					if !down {
						svg += &sides[1].width(2.0).color(COLORS[idx % COLORS.len()]).flip_to(x_start, 0).to_string();
					}
				} else {
					svg += &sides[2].width(2.0).color(COLORS[idx % COLORS.len()]).to_string();
				}
				if right {
					svg += &sides[3].width(1.0).color(white()).to_string();
					if !up {
						svg += &sides[0].width(2.0).color(COLORS[idx % COLORS.len()]).flip_to(x_end, 0).to_string();
					}
					if !down {
						svg += &sides[1].width(2.0).color(COLORS[idx % COLORS.len()]).flip_to(x_end, 0).to_string();
					}
				} else {
					svg += &sides[3].width(2.0).color(COLORS[idx % COLORS.len()]).to_string();
				}
			}
		}
		svg += &text((x+1) * size_factor - size8, (y+1) * size_factor - size4 / 3, i.to_string())
			.align(Align::Right)
			.size(SIZE_FONT)
			.color(black()).to_string();
		svg += &text(x * size_factor + size4, (y+1) * size_factor - size4, function[i].to_string())
			.size(SIZE_FONT * 2.0)
			.color(black()).to_string();
	}
	svg += &EndSvg.to_string();

	svg
}
use kv::*;

fn main() {
	let function = vec![0, 0, 1, 1, 1, 1, 1, 0].into_iter().map(Into::into).collect();
	let groups = find_groups(&function, &[A, B, C], One);
	println!("all:");
	for x in &groups {
		println!("{}", print_implicant(x));
	}
	println!("prime:");
	let (prime, other) = find_core(&function, &[A, B, C], One, &groups);
	for x in &prime {
		println!("{}", print_implicant(x));
	}
	println!("solutions:");
	let solutions = all_solutions(function, &[A, B, C], One, &prime, &other);
	for sol in solutions {
		for x in &prime {
			print!("{} | ", print_implicant(x));
		}
		for x in &sol {
			print!("{} | ", print_implicant(x));
		}
		println!();
	}
}

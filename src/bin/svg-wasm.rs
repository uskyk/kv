use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use log::{Level, info};

use kv::*;
use web_sys::{EventTarget, HtmlElement, HtmlInputElement, HtmlSelectElement, HtmlTextAreaElement};

macro_rules! web {
	($($x:ident)*) => {
		web_sys::window().unwrap()$(.$x().unwrap())*
	}
}

macro_rules! web2 {
	($($x:ident)*) => {
		web_sys::window().unwrap()$(.$x())*
	}
}

const HINT_TEXT: &'static str = "Output: core blocks, prime blocks, solution 1, solution 2, ... (solutions only displayed if not equal to core or prime blocks)";

fn main() {
	console_log::init_with_level(Level::Debug).unwrap();
	std::panic::set_hook(Box::new(console_error_panic_hook::hook));
	info!("init");
	init_settings();
	if !parse_hash() {
		let grid = grid(get_var_number());
		update_input_grid(&grid);
	}
	let closure = Closure::wrap(Box::new(|| { parse_hash(); }) as Box<dyn FnMut()>);
	web!().add_event_listener_with_callback("hashchange", closure.as_ref().unchecked_ref()).unwrap();
	closure.forget();
}

fn parse_hash() -> bool {
	let hash = web!(document location hash);
	if hash.len() >= 2 {
		let func_spec = &hash[1..];
		let var_count = (func_spec.len() as f32).log2() as usize;
		if 2usize.pow(var_count as u32) == func_spec.len() {
			set_var_number(var_count);
			let grid = grid(var_count);
			update_input_grid(&grid);
			set_input_function(func_spec);
			return true;
		}
	}
	false
}

#[wasm_bindgen(module = "/base.js")]
extern "C" {
	pub fn attachTypeListener(element: &HtmlElement);
}

fn get_output_container() -> HtmlElement {
	web!(document).get_element_by_id("output-container").unwrap().unchecked_into::<HtmlElement>()
}

fn get_output_help() -> HtmlElement {
	web!(document).get_element_by_id("output-help").unwrap().unchecked_into::<HtmlElement>()
}

fn p(text: &str) -> HtmlElement {
	let p = web!(document).create_element("p").unwrap().unchecked_into::<HtmlElement>();
	p.set_inner_text(text);
	p
}

fn span(text: &str) -> HtmlElement {
	let p = web!(document).create_element("span").unwrap().unchecked_into::<HtmlElement>();
	p.set_inner_text(text);
	p
}

pub fn run() {
	let document = web!(document);

	let print_labels = get_labels();
	let mode = get_mode();
	let var_count = get_var_number();
	let vars = (0..var_count).map(|x| 2usize.pow(x as u32)).collect::<Vec<_>>();
	let function: FunctionSpec = (0..2usize.pow(var_count as u32)).map(|idx| get_input(idx)).collect();
	info!("function: {:?}", function);
	let hash = Some('#').into_iter().chain(function.iter().map::<char, _>(|&x| x.into())).collect::<String>();
	web2!(location).set_hash(&hash).unwrap();
	let groups = find_groups(&function, &vars, mode);
	let (prime, other) = find_core(&function, &vars, mode, &groups);
	let solutions = real_solutions_idxed(function.clone(), &vars, mode, &prime, &other);

	let output_container = get_output_container();
	if output_container.children().length() == 0 {
		let output_help = get_output_help();
		output_help.append_child(&p(HINT_TEXT)).unwrap();
		output_container.append_child(&document.create_element("hr").unwrap()).unwrap();
	}
	let output_sub = document.create_element("div").unwrap().unchecked_into::<HtmlElement>();

	let (_grid, w, h) = grid(var_count);

	macro_rules! render_svg {
		($sol:expr, $mask:expr) => {
			let svg = svg::get_svg(if var_count < 5 { 64 } else { 80 }, print_labels, &function, &vars, $mask);
			let svg_container = document.create_element("div").unwrap();
			svg_container.set_class_name("solution");
			svg_container.set_inner_html(&svg);
			let svg = svg_container.children().item(0).unwrap().unchecked_into::<HtmlElement>();
			svg.style().set_property("width", &format!("{}px", w * 80)).unwrap();
			svg.style().set_property("height", &format!("{}px", h * 80)).unwrap();
			let mut text = if mode == Output::One { print_df($sol) } else { print_mf($sol) };
			if text.is_empty() {
				text += ".";
			}
			svg_container.append_child(&span(&text)).unwrap();
			output_sub.append_child(&svg_container).unwrap();
		};
	}

	let mut block_masks: Vec<_> = prime.iter()
		.map(block_to_mask)
		.collect();
	render_svg!(prime.iter().map(|x| *x), &block_masks);

	block_masks = prime.iter()
		.map(block_to_mask)
		.chain(other.iter().map(block_to_mask)) // also display non-core
		.collect();
	render_svg!(prime.iter().chain(other.iter()).map(|x| *x), &block_masks);

	let mut size = usize::MAX;
	for sol in &solutions {
		if sol.len() > size {
			break;
		}
		size = sol.len();
		block_masks = prime.iter()
			.map(block_to_mask)
			.chain((0..other.len()).map(|idx| {
				if sol.contains(&idx) {
					block_to_mask(&other[idx])
				} else {
					(!0, 0)
				}
			}))
			.collect();
		let blocks = prime.iter().map(|x| *x)
			.chain(sol.iter().map(|&i| other[i]));
		render_svg!(blocks, &block_masks);
	}

	output_container.prepend_with_node_1(&output_sub).unwrap();
	output_container.prepend_with_node_1(&document.create_element("hr").unwrap()).unwrap();
}

fn set_var_number(x: usize) {
	get_var_number_el().set_value(&x.to_string());
}

fn init_settings() {
	let set0 = web!(document).get_element_by_id("set-0").unwrap().unchecked_into::<EventTarget>();
	let closure = Closure::wrap(Box::new(|| set_input_function(&"0".repeat(2usize.pow(get_var_number() as u32)))) as Box<dyn FnMut()>);
	set0.add_event_listener_with_callback("click", closure.as_ref().unchecked_ref()).unwrap();
	closure.forget();
	let set1 = web!(document).get_element_by_id("set-1").unwrap().unchecked_into::<EventTarget>();
	let closure = Closure::wrap(Box::new(|| set_input_function(&"1".repeat(2usize.pow(get_var_number() as u32)))) as Box<dyn FnMut()>);
	set1.add_event_listener_with_callback("click", closure.as_ref().unchecked_ref()).unwrap();
	closure.forget();
	let seta = web!(document).get_element_by_id("set-any").unwrap().unchecked_into::<EventTarget>();
	let closure = Closure::wrap(Box::new(|| set_input_function(&"-".repeat(2usize.pow(get_var_number() as u32)))) as Box<dyn FnMut()>);
	seta.add_event_listener_with_callback("click", closure.as_ref().unchecked_ref()).unwrap();
	closure.forget();
	let calculate = web!(document).get_element_by_id("calculate").unwrap().unchecked_into::<EventTarget>();
	let closure = Closure::wrap(Box::new(|| run()) as Box<dyn FnMut()>);
	calculate.add_event_listener_with_callback("click", closure.as_ref().unchecked_ref()).unwrap();
	closure.forget();
	let count = web!(document).get_element_by_id("variables").unwrap().unchecked_into::<EventTarget>();
	let closure = Closure::wrap(Box::new(|| update_input_grid(&grid(get_var_number()))) as Box<dyn FnMut()>);
	count.add_event_listener_with_callback("change", closure.as_ref().unchecked_ref()).unwrap();
	closure.forget();
}

fn get_var_number_el() -> HtmlInputElement {
	web!(document).get_element_by_id("variables").unwrap().unchecked_into()
}

fn get_var_number() -> usize {
	get_var_number_el().value().parse().unwrap()
}

fn get_labels_el() -> HtmlInputElement {
	web!(document).get_element_by_id("labels").unwrap().unchecked_into()
}

fn get_labels() -> bool {
	get_labels_el().checked()
}

fn get_mode_el() -> HtmlSelectElement {
	web!(document).get_element_by_id("mode-select").unwrap().unchecked_into()
}

fn get_mode() -> Output {
	match &*get_mode_el().value() {
		"kmf" => Output::Zero,
		"dmf" | _ => Output::One,
	}
}

fn get_input_container() -> HtmlElement {
	web!(document).get_element_by_id("input-container").unwrap().unchecked_into::<HtmlElement>()
}

fn get_input_el(idx: usize) -> Option<HtmlElement> {
	web!(document).get_element_by_id(&format!("input{}", idx)).map(|x| x.unchecked_into::<HtmlElement>())
}

fn create_input_el(idx: usize, x: usize, y: usize) -> HtmlElement {
	let input = web!(document).create_element("textarea").unwrap().unchecked_into::<HtmlTextAreaElement>();
	input.set_attribute("id", &format!("input{}", idx)).unwrap();
	input.set_value("-");
	let style = input.style();
	style.set_property("position", "absolute").unwrap();
	style.set_property("left", &format!("{}px", x)).unwrap();
	style.set_property("top", &format!("{}px", y)).unwrap();
	get_input_container().append_child(&input).unwrap();
	attachTypeListener(&input);
	input.into()
}

fn get_input(idx: usize) -> Output {
	let el = get_input_el(idx).unwrap().unchecked_into::<HtmlTextAreaElement>();
	match &*el.value() {
		"0" => Zero,
		"1" => One,
		_ => Any
	}
}

fn set_input(idx: usize, out: Output) {
	let el = get_input_el(idx).unwrap().unchecked_into::<HtmlTextAreaElement>();
	el.set_value(&out.to_string());
}

fn set_input_function(func: &str) {
	for i in 0..func.len() {
		set_input(i, func[i..i+1].parse().unwrap());
	}
}

// also set in style
const GRID_SPACING: usize = 64;

fn update_input_grid((grid, w, h): &(Vec<(usize, usize)>, usize, usize)) {
	let input_container = get_input_container();
	input_container.style().set_property("width", &format!("{}px", w * 64)).unwrap();
	input_container.style().set_property("height", &format!("{}px", h * 64)).unwrap();
	for i in 0..grid.len() {
		if get_input_el(i).is_none() {
			create_input_el(i, grid[i].0 * GRID_SPACING, grid[i].1 * GRID_SPACING);
		}
	}
	for i in grid.len().. {
		if let Some(el) = get_input_el(i) {
			el.remove();
		} else {
			break;
		}
	}
}

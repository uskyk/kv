use std::env::args;

use kv::*;

const SIZE_FACTOR: usize = 64;

fn main() {
	let args = args().collect::<Vec<_>>();

	let vars = [A, B, C, D, E];
	//let function = vec![0, 0, 1, 1, 1, 1, 1, 0].into_iter().map(Into::into).collect();
	//let function = vec![0, 0, 1, 1, 1, 1, 1, 2].into_iter().map(Into::into).collect();
	//let function = parse_function("-1111000-1-01---");
	let function = parse_function("11101111111111111111101111111101");
	let groups = find_groups(&function, &vars, One);
	eprintln!("all:");
	for x in &groups {
		eprintln!("{}", print_implicant(x));
	}
	eprintln!("prime:");
	let (prime, other) = find_core(&function, &vars, One, &groups);
	for x in &prime {
		eprintln!("{}", print_implicant(x));
	}
	eprintln!("solutions:");
	let solutions = all_solutions(function.clone(), &vars, One, &prime, &other);
	for sol in &solutions {
		for x in &prime {
			eprint!("{} | ", print_implicant(x));
		}
		for x in sol {
			eprint!("{} | ", print_implicant(x));
		}
		eprintln!();
	}

	let block_masks: Vec<_> = if let Some(Ok(index)) = args.get(1).map(|x| x.parse::<usize>()) {
		prime.iter()
			.map(block_to_mask)
			.chain(solutions[index].iter().map(block_to_mask))
			.collect()
	} else {
		prime.iter()
			.map(block_to_mask)
			.chain(other.iter().map(block_to_mask))
			.collect()
	};

	println!("{}", svg::get_svg(SIZE_FACTOR, true, &function, &vars, &block_masks));
}

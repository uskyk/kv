#!/usr/bin/env bash
trunk build --release --public-url '/kv/'
wasm-opt -O4 dist/*.wasm -o=/tmp/wasm.opt && cp /tmp/wasm.opt dist/*.wasm
rm -r ../website/kv
cp --archive dist ../website/kv
